
#include "xblastlcd.h"
#include "conio.h"
#include "../dvd2xbox/d2xsettings.h"

/*
HD44780 or equivalent
Character located  1   2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20
DDRAM address      00 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f 10 11 12 13
DDRAM address      40 41 42 43 44 45 46 47 48 49 4a 4b 4c 4d 4e 4f 50 51 52 53
DDRAM address      14 15 16 17 18 19 1a 1b 1c 1d 1e 1f 20 21 22 23 24 25 26 27
DDRAM address      54 55 56 57 58 59 5a 5b 5c 5d 5e 5f 60 61 62 63 64 65 66 67
*/

#define XBLAST_IO_REG	    0xF70D
#define XBLAST_5V_IO_BIT    0x01

#define DISP_O_LIGHT        0xF701    // Display Port brightness control
#define DISP_O_CONTRAST     0xF703    // Display Port contrast control

#define CMD                 0x00

#define DISP_FUNCTION_SET   0x20  // cmd: set interface data length
#define DISP_N_FLAG         0x08  // number of display lines:2-line / 1-line

//*************************************************************************************************************
void CXBlastLCD::Initialize()
{
  StopThread();
  if (!g_d2xSettings.m_bLCDUsed)
  {
    /*CLog::Log(LOGINFO, "lcd not used");*/
    return;
  }
  EnableXBlast5V();
  Create();

}

//*************************************************************************************************************
void CXBlastLCD::Stop(bool TurnOffLCD)
{
  if(TurnOffLCD)
  {
	  DisableXBlast5V();
  }
  CSmartXXLCD::Stop(false);
}

//************************************************************************************************************************
//Set brightness level
//************************************************************************************************************************
void CXBlastLCD::DisplaySetBacklight(unsigned char level)
{
  if (g_d2xSettings.m_iLCDType==LCD_TYPE_VFD)
  {
    //VFD:(value 0 to 3 = 100%, 75%, 50%, 25%)
    if (level<0) level=0;
    if (level>99) level=99;
    level = (99-level);
    level/=25;
    DisplayOut(DISP_FUNCTION_SET | DISP_N_FLAG | level,CMD);
  }
  else //if (g_guiSettings.GetInt("lcd.type")==LCD_TYPE_LCD_HD44780)
  {
      float fBackLight=((float)level)/100.0f;
      fBackLight*=127.0f;
      int iNewLevel=(int)fBackLight;
      if (iNewLevel==63) iNewLevel=64;
     
      // Set new value
      _outp(DISP_O_LIGHT, iNewLevel&127);

  }
}
//************************************************************************************************************************
//Set Contrast level
//************************************************************************************************************************
void CXBlastLCD::DisplaySetContrast(unsigned char level)
{
  // can't set contrast with a VFD
  if (g_d2xSettings.m_iLCDType==LCD_TYPE_VFD)
    return;

	float fBackLight=((float)level)/100.0f;
	fBackLight*=127.0f;
	int iNewLevel=(int)fBackLight;
	if (iNewLevel==63) iNewLevel=64;
    _outp(DISP_O_CONTRAST, iNewLevel&127);
}

void CXBlastLCD::EnableXBlast5V()
{
	unsigned char XblastIORegValue = _inp(XBLAST_IO_REG);

	if(!(XblastIORegValue & XBLAST_5V_IO_BIT))
	{
		_outp(XBLAST_IO_REG, (XblastIORegValue | XBLAST_5V_IO_BIT));
	}
}

void CXBlastLCD::DisableXBlast5V()
{
	unsigned char XblastIORegValue = _inp(XBLAST_IO_REG);
	
	if(XblastIORegValue & XBLAST_5V_IO_BIT)
	{
		_outp(XBLAST_IO_REG, (XblastIORegValue & ~XBLAST_5V_IO_BIT));
	}
}
