#pragma once

#include "../smartXX/smartxxlcd.h"

class CXBlastLCD : public CSmartXXLCD
{
public:
  virtual void Initialize();
  virtual void Stop(bool TurnOffLCD);

protected:

  void    DisplaySetBacklight(unsigned char level) ;
  void    DisplaySetContrast(unsigned char level) ;

  void EnableXBlast5V();
  void DisableXBlast5V();
};
